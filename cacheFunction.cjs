// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {
  try {
    const cache = {};
    return function (...args) {
      const key = JSON.stringify(args);
      if (cache.hasOwnProperty(key)) {
        console.log("Returning cached result");
        return cache[key];
      } else {
        const result = cb(...args);
        cache[key] = result;
        console.log("Caching result and returning it");
        return result;
      }
    };
  } catch (error) {
    console.error("Error is:", error.message);
  }
}


module.exports = cacheFunction;

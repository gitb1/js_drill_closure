// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.

function counterFactory() {
  try {
    let counter = 0;

    return {
      increment() {
        return ++counter;
      },
      decrement() {
        return --counter;
      },
    };
  } catch (error) {
    console.error("Error is:", error.message);
  }
}

module.exports = counterFactory;

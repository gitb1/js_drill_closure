// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned times.

// Returning null is acceptable if cb can't be returned
function limitFunctionCallCount(cb, n) {
  try {
    let count = 0;

    return function (...args) {
      if (count < n) {
        count++;
        return cb(...args);
      } else {
        return null;
      }
    };
  } catch (error) {
    console.error("Error is:", error.message);
  }
}

module.exports = limitFunctionCallCount;

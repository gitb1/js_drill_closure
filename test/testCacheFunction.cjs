const cacheFunction = require("../cacheFunction.cjs");


try {
  const memoizedFunction = cacheFunction(function (x, y) {
    return x + y;
  });
  console.log(memoizedFunction(2, 3));
  console.log(memoizedFunction(2, 3));
  console.log(memoizedFunction(4, 5));
  console.log(memoizedFunction(2, 3));
} catch (error) {
  console.error("Error is:", error.message);
}

const counterFactory = require("../counterFactory.cjs");

try {
  const countObj = counterFactory();

  console.log(countObj.increment());
  console.log(countObj.increment());
  console.log(countObj.decrement());
  console.log(countObj.decrement());
} catch (error) {
  console.error("Error is:", error.message);
}

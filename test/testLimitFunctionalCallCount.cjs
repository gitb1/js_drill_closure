const limitFunctionCallCount = require("../limitFunctionalCallCount.cjs");
try {
  const limitedFunction = limitFunctionCallCount(function () {
    console.log("Callback function invoked");
  }, 3);
  limitedFunction();
} catch (error) {
  console.error("Error is: ", error.message);
}
